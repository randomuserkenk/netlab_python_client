import requests, json, os
from datetime import datetime
from pymongo import MongoClient as Mongo
import logging

class Client(object):

    collection_name_relation = [
        ('Распродажа', 'sales_products'),
        ('Прайс-лист', 'price_products'),
        ('В наличии', 'stock_products'),
        ('Новинки', 'new_products'),
    ]

    def __updateToken(self):
        try:
            req = requests.get(self.URLS['AUTH'], params = {
                'username': self.username,
                'password': self.password })
            req = json.loads(req.text.split('{} && ')[1])                       # Косяк netlab - выдает лишние в Json ответе
            if req['tokenResponse']['status']['code'] == '200':
                self.token = req['tokenResponse']['data']['token']
                self.expired_token = datetime.strptime(req['tokenResponse']['data']['expiredIn'], '%d.%m.%Y %H:%M')
            else:
                raise Exception()
        except Exception as e:
            logging.exception("Exception {}".format(str(e)))

    def __requestCleaner(self, url, data = {}):

        self.__checkToken()
        data.update({'oauth_token': self.token})
        try:
            req = requests.get(url, params = data)
            req = json.loads(req.text.split('{} && ')[1])                       # Косяк netlab - выдает лишние в Json ответе
            return req
        except Exception as e:
            logging.exception("Exception {}".format(str(e)))

    def __checkToken(self):
        if self.expired_token < datetime.now():
            self.__updateToken()

    def __init__(self):
        self.username = os.environ.get('NETLAB_UNAME')
        self.password = os.environ.get('NETLAB_PWD')
        self.URLS = {
            'AUTH': 'http://services.netlab.ru/rest/authentication/token.json',
            'CATALOG': 'http://services.netlab.ru/rest/catalogsZip/list.json',
            'CATALOG_PRODUCTS': 'http://services.netlab.ru/rest/catalogsZip/{}.json',
            'PRODUCTS': 'http://services.netlab.ru/rest/catalogsZip/{}/{}.json'
            'PRODUCT': 'http://services.netlab.ru/rest/catalogsZip/{}/{}/{}.json'
            }
        self.token = str()
        self.expired_token = datetime.now()
        db_client = Mongo(
            os.environ.get('MONGO_IP'), 27017,
            username = os.environ.get('MONGO_UNAME'),
            password = os.environ.get('MONGO_PWD'))
        self.db = db_client.netlab
        self.__updateToken()

    def getMainCatalog(self):
        return self.__requestCleaner(self.URLS['CATALOG'])

    def getCategories(self, relation):
        return list(self.db['catalogs'].find({'name': relation[0]}))[0]['category']

    def getProductByID(self, relation = self.collection_name_relation[1], category_id, product_id):
        return self.__requestCleaner(self.URL['PRODUCT'].format(relation[0], category_id, product_id))

    def getProducts(relation, product_id):
        return self.__requestCleaner(self.URLS['PRODUCTS'].format(relation[0], product_id))

    def saveMainCatalog(self):
        '''
        Получение общего каталога.
        !!! Не содержит продукты
        '''
        req = self.getMainCatalog()
        try:
            self.db['catalogs'].insert_one(req['catalogsResponse']['data'])
        except Exception as e:
            logging.exception("Exception {}".format(str(e)))

    def saveCatalogProductPart(self, relation):
        '''
        Заполнение каталога продуктами почастично.
        В relation передается один из элементов self.collection_name_relation.
        '''
        req = self.__requestCleaner(self.URLS['CATALOG_PRODUCTS'].format(relation[0]))
        try:
            self.db['catalogs'].insert_one(req['catalogResponse']['data'])
        except Exception as e:
            logging.exception("Exception {}".format(str(e)))

    def saveFullCatalog(self):
        '''
        Заполнение каталога как категорий так и частичного описания товаров.
        '''
        self.getMainCatalog()
        for relation in self.collection_name_relation:
            self.saveCatalogProductPart(relation)


    def saveProductsByCategory(self, relation):
        '''
        Сохраняем каждый продукт из категорий self.collection_name_relation,
        элемент которого передается в relation.
        '''
        ids = self.getCategories(relation)
        for item in ids:
            if not self.db[relation[1]].find({'id': item['id']}).count():
                req = self.getProducts(relation, item['id'])
                try:
                    self.db[relation[1]].insert_one(req['categoryResponse']['data'])
                except Exception as e:
                    logging.exception("Exception {}".format(str(e)))
